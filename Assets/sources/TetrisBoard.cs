﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  TetrisBoard.cs
//  Language: C#
//
//  Description:
//  Script that handles the blocks on the tetris grid, collision, and adding pieces.
// ******************************

using System;

using UnityEngine;

/*
 * Class that controls the tetris board 
 */
public class TetrisBoard{

    public static int width = 14;
    public static int height = 24;

    public GameObject[,] grid;

	int cheatWeight;
	int cheatNumber;

	/*
     * Use this for initialization
     */
    public TetrisBoard(){
        grid = new GameObject[width, height];

		if(GameControler.gameControler.gameSettings.cheatNextBlockEnabled) {
			cheatWeight += GameControler.gameControler.gameSettings.cheatNextBlockWeight;
			cheatNumber++;
		}
		if(GameControler.gameControler.gameSettings.cheatReverseBoardEnabled) {
			cheatWeight += GameControler.gameControler.gameSettings.cheatReverseBoardWeight;
			cheatNumber++;
		}
		if(GameControler.gameControler.gameSettings.cheatRemoveBlocksEnabled) {
			cheatWeight += GameControler.gameControler.gameSettings.cheatRemoveBlocksWeight;
			cheatNumber++;
		}
	}

	/*
	 * Reset the tetris grid by removing any blocks
	 */
    public void Reset(){
        foreach(GameObject place in grid)
            UnityEngine.Object.Destroy(place);
    }

	/*
	 * See if the piece has collided on the bottom with the screen or blocks
	 */
    public bool Collision(ActiveGroup piece){
        foreach(GameObject block in piece.blocks)
            if(Math.Round(block.transform.position.y) <= 0)
                return true;

        // We can do this safely because y > 0 is determined by first check
        foreach(GameObject block in piece.blocks)
            if(grid[(int)Math.Round(block.transform.position.x), (int)Math.Round(block.transform.position.y) - 1] != null)
                return true;

        return false;
    }

	/*
	 * See if the piece collide with the left side of the screen
	 * or blocks
	 */
    public bool CollideLeft(ActiveGroup piece){
        foreach(GameObject block in piece.blocks)
            if(Math.Round(block.transform.position.x) <= 0)
                return true;

        foreach(GameObject block in piece.blocks)
            if(grid[(int)Math.Round(block.transform.position.x) - 1, (int)Math.Round(block.transform.position.y)] != null)
                return true;

        return false;
    }

	/*
	 * See if the piece collides with the right side of the screen
	 * or blocks
	 */
    public bool CollideRight(ActiveGroup piece){
        foreach(GameObject block in piece.blocks)
            if(Math.Round(block.transform.position.x) >= width - 1)
                return true;

        foreach(GameObject block in piece.blocks)
            if(grid[(int)Math.Round(block.transform.position.x) + 1, (int)Math.Round(block.transform.position.y)] != null)
                return true;

        return false;
    }

	/*
	 * Check that the piece can be rotated, shunt it over to keep it
	 * in bounds, or fail to rotate
	 */
    public bool PlaceRotation(ActiveGroup piece){
        int shuntX = 0;
        int shuntY = 0;
        foreach(GameObject block in piece.blocks){
            if(Math.Round(block.transform.position.x) >= width - 1){
                int tempShunt = (int)Math.Round(block.transform.position.x) - (width - 1);
                if(tempShunt > shuntX)
                    shuntX = tempShunt;
            }
            if(Math.Round(block.transform.position.x) <= 0){
                int tempShunt = (int)Math.Round(block.transform.position.x) - 1;
                if(tempShunt < shuntX)
                    shuntX = tempShunt;
            }

            if(Math.Round(block.transform.position.y) <= 0){
                int tempShunt = (int)Math.Round(block.transform.position.y) - 1;
                if(tempShunt < shuntY)
                    shuntY = tempShunt;
            }
        }
        // Move in bounds, We cant move like this!!!!! it is a diagonal!
        piece.MovePosition(new Vector3(-shuntX, 0, 0));
        piece.MovePosition(new Vector3(0, -shuntY, 0));

        //are we colliding with anythin here?
        foreach(GameObject block in piece.blocks){
            if(grid[(int)Math.Round(block.transform.position.x), (int)Math.Round(block.transform.position.y)] != null){
                piece.MovePosition(new Vector3(shuntX, 0, 0)); //move back, move not legal
                piece.MovePosition(new Vector3(0, shuntY, 0)); //move back, move not legal
                return false;
            }
        }

        return true;
    }

	/*
	 * Add a piece to the grid
	 */
    public void AddPiece(ActiveGroup piece){
        foreach(GameObject block in piece.blocks)
            grid[(int)Math.Round(block.transform.position.x), (int)Math.Round(block.transform.position.y)] = block;

        CheckRows();
    }

	/*
	 * See if there are any completed rows
	 */
    void CheckRows(){
        for(int y = 0; y < height; y++){
            if(IsRowFull(y)){
                DeleteRow(y);
                RippleDown(y + 1);
                --y;
            }
        }    
    }

	/*
	 * See if the current row is completed
	 */
    bool IsRowFull(int y){
        for(int x = 0; x < width; x++)
            if(grid[x, y] == null)
                return false;
        return true;
    }

	/*
	 * Delete the row at height y
	 */
    void DeleteRow(int y){
        GameControler.gameControler.PlayBlockSound();
        GameControler.gameControler.gameSettings.score += 100;

        int r = UnityEngine.Random.Range(0, cheatWeight);
		int weightCurrent = 0;

		if(GameControler.gameControler.gameSettings.cheatNextBlockEnabled && r <= (weightCurrent += GameControler.gameControler.gameSettings.cheatNextBlockWeight)){
            GameControler.gameControler.gameSettings.cheatNextBlock = true;
            Debug.Log("Cheating next block");
		}else if(GameControler.gameControler.gameSettings.cheatReverseBoardEnabled && r <= (weightCurrent += GameControler.gameControler.gameSettings.cheatReverseBoardWeight)){
            Debug.Log("Cheating Reversing board");
            ReverseBoard();
		}else if(GameControler.gameControler.gameSettings.cheatRemoveBlocksEnabled && r <= (weightCurrent += GameControler.gameControler.gameSettings.cheatRemoveBlocksWeight)){
            Debug.Log("Cheating Removing blocks");
            CheatRemove();
        }
		weightCurrent = 0;

        for(int x = 0; x < width; x++){
            UnityEngine.Object.Destroy(grid[x, y]);
            grid[x, y] = null;
        }
    }

    /*
     * Invert grid on x axis
     */
    void ReverseBoard(){
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width/2; x++){
                GameObject temp = grid[x + width/2, y];
                grid[x + width/2, y] = grid[x, y];
                grid[x, y] = temp;

                if(grid[x, y] != null){
                    grid[x, y].transform.position = new Vector3(x, y, 0);
                }

                if(grid[x + width / 2, y] != null){
                    grid[x + width / 2, y].transform.position = new Vector3(x + width / 2, y, 0);
                }
            }
        }
    }

	/*
	 * Choose 10 random values and remove them
	 */
    void CheatRemove(){
        for(int i = 0; i < 10; i++){
            int y = UnityEngine.Random.Range(0, height);
            int x = UnityEngine.Random.Range(0, width);
            if(grid[x, y] != null){
				UnityEngine.Object.Destroy(grid[x, y]);
                grid[x, y] = null;
            }
        }
    }

	/*
	 * Move rows down
	 */
    void RippleDown(int y){
        for(int i = y; i < height; i++){
            for(int x = 0; x < width; x++){
                if(grid[x, i] != null){
                    grid[x, i - 1] = grid[x, i];
                    grid[x, i] = null;
                    //Actually move the block down
                    grid[x, i - 1].transform.position += Vector3.down;
                }
            }
        }
    }
}
