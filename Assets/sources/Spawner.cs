﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  Spawner.cs
//  Language: C#
//
//  Description:
//  Script that defines the spawning behavior
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;

using System;

/*
 * Class that controls the spawning object
 */
public class Spawner : MonoBehaviour {
    public GameObject[] blocks;  //Block prefabs
    public GameObject gamePiece; //Piece prefab

    static Spawner self;

    GameObject previewArea;

    static ActiveGroup activeGroup;
    static ActiveGroup previewGroup;

	/*
	 * Make this a sincleton object so we don't have confusing logic
	 * after switching scenes
	 */
    void Awake(){
        if(self == null){
            DontDestroyOnLoad(gameObject);
            self = this;
        }else if(self != this){
            Destroy(gameObject);
        }

        previewArea = GameObject.FindWithTag("PreviewTag");

        if(GameControler.gameControler.paused && GameControler.gameControler.newGame){
			// We have a game running and are starting over
            activeGroup.DestroyPiece();
            previewGroup.DestroyPiece();
            GameControler.gameControler.newGame = false;
            GameControler.gameControler.paused = false;
            TetrisStart();
        }else if(GameControler.gameControler.paused){
			// We have a game running and are returning to it
            UnPause();
        }else if(GameControler.gameControler.newGame){
			// We don't have a game running yet
            GameControler.gameControler.newGame = false;
            TetrisStart();
        }
    }

	/*
	 * Every Frame check if the main menu key has been pressed
	 */
	void Update() {
		if(Input.GetKeyDown(GameControler.gameControler.gameSettings.mainMenuKey)) {
			Pause();
		}
	}

    /*
     * This is called on load
     */
    void TetrisStart(){
        // Set the first piece
        int i = UnityEngine.Random.Range(0, blocks.Length);
        previewGroup = GetNextPiece(blocks[i]);
        Preview(previewGroup);

        // We can now grab our first piece from preview and continue
        SpawnNext();
	}

	/*
	 * If the application looses focus pause the game
	 */
    void OnApplicationFocus(bool hasFocus){
        if(!hasFocus)
            Pause();
    }

	/*
	 * If the application gets a pause signal pause the game
	 * This is more of a mobile thing but put it here anyway
	 */
    void OnApplicationPause(bool pauseStatus){
        if(pauseStatus)
            Pause();
    }

	/*
	 * Start up the game again
	 */
    void UnPause(){
        //Debug.Log("Trying to unpause");
        GameControler.gameControler.paused = false;
        activeGroup.enabled = true;
    }

	/*
	 * Stop the pieces then move to the main menu
	 */
    void Pause(){
        GameControler.gameControler.paused = true;
        if(activeGroup != null)
            activeGroup.enabled = false;
        SceneManager.LoadScene("MainMenu");
    }

    /*
     * Move block from preview area to spawner and activate 
     */
    public void SpawnNext(){
        int i = UnityEngine.Random.Range(0, blocks.Length); //choose what blocks this piece uses

        if(GameControler.gameControler.gameSettings.cheatNextBlock){
            activeGroup = GetNextPiece(blocks[i]);
            Spawn(activeGroup);
            GameControler.gameControler.gameSettings.cheatNextBlock = false;
        }else{
            activeGroup = previewGroup;
            previewGroup = GetNextPiece(blocks[i]);
            Preview(previewGroup);
            Spawn(activeGroup);
        }
    }

    /*
     * Get the next piece that will be spawned
     */
    ActiveGroup GetNextPiece(GameObject blockPrefab){
        ActiveGroup nextPiece = Instantiate(gamePiece).GetComponent<ActiveGroup>();
        nextPiece.CreatePiece(blockPrefab);
        return nextPiece;
    }

    /*
     * Move block to Spawn area
     */
    void Spawn(ActiveGroup aGroup){
        aGroup.SetPosition(gameObject.transform.position);
        if(GameControler.gameControler.tetrisBoard.Collision(aGroup)){
            GameOver();
        }else{
            aGroup.enabled = true;
        }
    }

    /*
     * Move block to preview area
     */
    void Preview(ActiveGroup aGroup){
        aGroup.enabled = false;
        if(previewArea == null)
            previewArea = GameObject.FindWithTag("PreviewTag");
        aGroup.SetPosition(previewArea.gameObject.transform.position);
    }

	/*
	 * What to do when a Game Over condition is meet
	 */
    void GameOver(){
        //Debug.Log("Game Over");

		int score = GameControler.gameControler.gameSettings.score;
		int[] scores = GameControler.gameControler.gameSettings.leaderScores;
		string[] times = GameControler.gameControler.gameSettings.leaders;

		for(int i = 0; i < 5; i++) {
			if(score > scores[i]) {
				//ripple scores
				for(int j = 4; j > i; j--) {
					scores[j] = scores[j - 1];
				}
				scores[i] = score;

				//ripple times
				for(int j = 4; j > i; j--) {
					times[j] = times[j - 1];
				}
				times[i] = DateTime.Now.ToString();
				GameControler.gameControler.highScore = i;
				break;
			}
		}

        GameControler.gameControler.tetrisBoard.Reset();
        GameControler.gameControler.gameSettings.score = 0;
        GameControler.gameControler.gameSettings.cheatPoints = 0;
		GameControler.gameControler.paused = false;
        activeGroup.DestroyPiece();
        previewGroup.DestroyPiece();

        if(GameControler.gameControler.highScore > -1){
            SceneManager.LoadScene("LeaderBoardMenu");
        }else{
            SceneManager.LoadScene("MainMenu");
        }
    }
}
