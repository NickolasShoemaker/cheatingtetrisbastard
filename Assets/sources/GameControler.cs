﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  GameControler.cs
//  Language: C#
//
//  Description:
//  Script that controls the settings for the entire game
// ******************************

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using UnityEngine;

/*
 * Class that controls the game
 */
public class GameControler : MonoBehaviour {

    //Static self reference
    public static GameControler gameControler;
    public TetrisBoard tetrisBoard;
    public GameSettings gameSettings;

    public AudioClip blockSound;
    public AudioSource audioSource;

    public bool paused;
    public bool newGame;
	public int highScore = -1;

	/*
	 * Load saved data if available 
	 */
	void Awake(){
        if(gameControler == null){
            DontDestroyOnLoad(gameObject);
            gameControler = this;
            gameSettings = new GameSettings();
            tetrisBoard = new TetrisBoard();
            paused = false;
            newGame = false;

            audioSource = GetComponent<AudioSource>();

            Load();
        }else if(gameControler != this){
            Destroy(gameObject);
        }
	}

	/*
	 * Keep data between scenes
	 */
	void Start(){
        audioSource.volume = gameSettings.musicVolume;
	}

	/*
	 * Save Game settings
	 */
    public void Save(){
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/GameController.dat");
        gameSettings.musicVolume = audioSource.volume;

        binaryFormatter.Serialize(file, gameSettings);
        file.Close();
    }

	/*
	 * Load the game settings
	 */
    public void Load(){
        if(File.Exists(Application.persistentDataPath + "/GameController.dat")){
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameController.dat", FileMode.Open);

            gameSettings = (GameSettings)binaryFormatter.Deserialize(file);
            file.Close();
            audioSource.volume = gameSettings.musicVolume;
        }else{
            gameSettings.Defaults();
        }
    }

	/*
	 * Save the settings when we quit
	 */
    void OnApplicationQuit(){
        Save();
    }

	/*
	 * Play Block Sound
	 */
    public void PlayBlockSound(){
        audioSource.PlayOneShot(blockSound, gameSettings.sfxVolume);
    }
}

/*
 * A serializable class to save all the game settings
 */
[System.Serializable]
public class GameSettings{
    //score
    public int score;
    public int cheatPoints;

	//settings
    public float musicVolume;
    public float sfxVolume;

	public float speedValue;
	public int speedOption;

	public bool fullScreen;
	public int resolutionIndex;

    //game cheat flags
    public bool cheatNextBlock;
    public bool cheatReverseBoard;

	public bool cheatNextBlockEnabled;
	public bool cheatReverseBoardEnabled;
	public bool cheatRemoveBlocksEnabled;

	public int cheatNextBlockWeight;
	public int cheatReverseBoardWeight;
	public int cheatRemoveBlocksWeight;

    //keys
    public KeyCode rotateKey;
    public KeyCode downKey;
    public KeyCode leftKey;
    public KeyCode rightKey;
    public KeyCode fastDownKey;
    public KeyCode mainMenuKey;

    //leader board
    public string[] leaders = new string[5];
    public int[] leaderScores = new int[5];

	/*
	 * Set all the values to something for the first time
	 */
    public void Defaults(){
	    score = 0;
	    cheatPoints = 0;
	    cheatNextBlock = false;

        musicVolume = 1.0f;
        sfxVolume = 1.0f;

		speedValue = 0.3f;
		speedOption = 2;

		fullScreen = true;
		resolutionIndex = -1;

		cheatNextBlockEnabled = true;
		cheatReverseBoardEnabled = true;
		cheatRemoveBlocksEnabled = true;

		cheatNextBlockWeight = 10;
		cheatReverseBoardWeight = 10;
		cheatRemoveBlocksWeight = 10;

        DefaultKeys();
		DefaultLeaderBoard();
    }

	/*
	 * Blank Leader Board
	 */
	public void DefaultLeaderBoard(){
		for(int i = 0; i < 5; i++)
			leaders[i] = "NA";

		for(int i = 0; i < 5; i++)
			leaderScores[i] = 0;
	}

	/*
	 * Default keys
	 */
    public void DefaultKeys(){
        rotateKey = KeyCode.UpArrow;
        downKey = KeyCode.DownArrow;
        leftKey = KeyCode.LeftArrow;
        rightKey = KeyCode.RightArrow;
        fastDownKey = KeyCode.Space;
        mainMenuKey = KeyCode.Escape;
    }
}