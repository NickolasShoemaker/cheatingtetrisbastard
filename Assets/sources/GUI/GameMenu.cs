﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  GameMenu.cs
//  Language: C#
//
//  Description:
//  Script that handles game play settings
// ******************************

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Class to handle the game play settings of the game
 */
public class GameMenu : MonoBehaviour {

	/*
	 * Initialize dropdown settings and cheat toggles
	 */
	void Start() {
		Dropdown dd = FindObjectOfType<Canvas>().GetComponentInChildren<Dropdown>();
		dd.value = GameControler.gameControler.gameSettings.speedOption;

		Toggle[] toggles = FindObjectOfType<Canvas>().GetComponentsInChildren<Toggle>();
		foreach(Toggle toggle in toggles) {
			if(toggle.name == "ToggleCheatNextPiece")
				toggle.isOn = GameControler.gameControler.gameSettings.cheatNextBlockEnabled;
			if(toggle.name == "ToggleCheatReverseBoard")
				toggle.isOn = GameControler.gameControler.gameSettings.cheatNextBlockEnabled;
			if(toggle.name == "ToggleCheatRemoveBlocks")
				toggle.isOn = GameControler.gameControler.gameSettings.cheatNextBlockEnabled;
		}
	}

	/*
	 * Update speed setting when speed dropdown is changed
	 */
	public void OnSpeedChanged(int value) {
		switch(value) {
			case 0://very slow 1.0f
				GameControler.gameControler.gameSettings.speedValue =  1.0f;
				GameControler.gameControler.gameSettings.speedOption = 0;
				break;
			case 1://slow 0.6f
				GameControler.gameControler.gameSettings.speedValue =  0.6f;
				GameControler.gameControler.gameSettings.speedOption = 1;
				break;
			case 2://normal 0.3f
				GameControler.gameControler.gameSettings.speedValue =  0.3f;
				GameControler.gameControler.gameSettings.speedOption = 2;
				break;
			case 3://fast 0.01f
				GameControler.gameControler.gameSettings.speedValue = 0.01f;
				GameControler.gameControler.gameSettings.speedOption = 3;
				break;
			case 4://very fast 0.002f
				GameControler.gameControler.gameSettings.speedValue = 0.002f;
				GameControler.gameControler.gameSettings.speedOption = 4;
				break;
			default:
				Debug.LogError("Speed set to unexpected option " + value.ToString() + ", 0 - 4 expected");
				break;
		}
	}

	/*
	 * Update game setting when cheat is toggled
	 */
	public void ToggleCheatNextBlockEnabled(bool value) {
		GameControler.gameControler.gameSettings.cheatNextBlockEnabled = value;
	}

	/*
	 * Update game setting when cheat is toggled
	 */
	public void ToggleCheatReverseBoardEnabled(bool value){
		GameControler.gameControler.gameSettings.cheatReverseBoardEnabled = value;
	}

	/*
	 * Update game setting when cheat is toggled
	 */
	public void ToggleCheatRemoveBlocksEnabled(bool value) {
		GameControler.gameControler.gameSettings.cheatRemoveBlocksEnabled = value;
	}

	/*
	 * On back button pressed go to main menu
	 */
	public void OnBack() {
		GameControler.gameControler.Save();
		SceneManager.LoadScene("MainMenu");
	}
}
