﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  GUIMaster.cs
//  Language: C#
//
//  Description:
//  Helper script for positioning GUI elements
// ******************************

using UnityEngine;

/*
 * Helper class for positioning GUI elements
 */
public static class GUIMaster{

	/*
	 * Place a Button at the bottom of the screen
	 */
	public static bool PlaceBottomButton(string text){
		GUIContent content = new GUIContent (text);
		Vector2 contentSize = GUI.skin.button.CalcSize (content);

		return GUI.Button (new Rect (Screen.width - (int)contentSize.x, Screen.height - (int)contentSize.y, (int)contentSize.x, (int)contentSize.y), text);
	}

	/*
	 * Place buttons at an x, y offset into the screen
	 */
	public static bool PlaceButton(int offsetX, int offsetY, string text){
		GUIContent content = new GUIContent (text);
		Vector2 contentSize = GUI.skin.button.CalcSize (content);

		return GUI.Button (new Rect (GetCenterWidth () - offsetX - contentSize.x / 2, GetCenterHeight () - offsetY - contentSize.y / 2, contentSize.x, contentSize.y), content);
	}

	/*
	 * Place labels at an x, y offset into the screen
	 */
	public static void PlaceLabel(int offsetX, int offsetY, string text){
		GUIContent content = new GUIContent (text);
		Vector2 contentSize = GUI.skin.button.CalcSize (content);

		GUI.Label (new Rect (GetCenterWidth () - offsetX - contentSize.x / 2, GetCenterHeight () - offsetY - contentSize.y / 2, contentSize.x, contentSize.y), content);
	}

	/*
	 * Get center of the screen
	 */
	public static int GetCenterWidth(){
		return Screen.width / 2;
	}

	/*
	 * Get center of the screen
	 */
	public static int GetCenterHeight(){
		return Screen.height / 2;
	}
}
