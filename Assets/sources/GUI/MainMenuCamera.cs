﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  MainMenuCamera.cs
//  Language: C#
//
//  Description:
//  Script that controls the main menu
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Class for the main menu
 */
public class MainMenuCamera : MonoBehaviour {

	/*
	 * Set the resolution of the window
	 */
	void Start() {
		//Set to last resolution on start
		int value = GameControler.gameControler.gameSettings.resolutionIndex;

		if(value != -1)
			Screen.SetResolution(Screen.resolutions[value].width ,Screen.resolutions[value].height ,GameControler.gameControler.gameSettings.fullScreen);
	}

	/*
	 * On resume button pressed go back to the game scene
	 */
	public void OnResume() {
		SceneManager.LoadScene("main");
	}

	/*
	 * On new game button pressed reset the tetris board and start the game
	 */
	public void OnNewGame() {
		GameControler.gameControler.tetrisBoard.Reset();
		GameControler.gameControler.gameSettings.score = 0;
		GameControler.gameControler.newGame = true;
		SceneManager.LoadScene("main");
	}

	/*
	 * When leader board button is pressed go to leader board menu
	 */
	public void OnLeaderBoard() {
		SceneManager.LoadScene("LeaderBoardMenu");
	}

	/*
	 * When options button is pressed go to options menu
	 */
	public void OnOptions() {
		SceneManager.LoadScene("OptionsMenu");
	}

	/*
	 * When quit is pressed close the program
	 */
	public void OnQuit() {
		Application.Quit();
	}
}
