﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  LeaderBoardMenu.cs
//  Language: C#
//
//  Description:
//  Script that displays the highest scores set in the game controler
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Class for the leader board
 */
public class LeaderBoardMenu : MonoBehaviour {

	Text[] scores;

	/*
	 * Get text UI elements
	 */
	void Start() {
		scores = FindObjectOfType<Canvas>().GetComponentsInChildren<Text>();
	}

	/*
	 * Every frame print the scores and check for main menu
	 */
	void Update() {
		GameSettings gs = GameControler.gameControler.gameSettings;

		if(Input.GetKeyDown(GameControler.gameControler.gameSettings.mainMenuKey)) {
			SceneManager.LoadScene("MainMenu");
		}

		for(int i = 0; i < 5; i++) {
			scores[i].text = gs.leaders[i] + " " + gs.leaderScores[i];
			if(i == GameControler.gameControler.highScore)
				scores[i].color = Color.green;
		}
	}

	/*
	 * On back button pressed go to main menu
	 */
	public void OnBack() {
		SceneManager.LoadScene("MainMenu");
	}

	/*
	 * On Reset button pressed reset the high scores
	 */
	public void OnReset() {
		GameControler.gameControler.gameSettings.DefaultLeaderBoard();
	}
}
