﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  ResumeButtonEnable.cs
//  Language: C#
//
//  Description:
//  Script that controls when the resume button on the main menu is enables or not
// ******************************

using UnityEngine;
using UnityEngine.UI;

/*
 * Class that conrols if the resume button on the main menu is enabled
 */
public class ResumeButtonEnable : MonoBehaviour {

	Selectable button;

	/*
	 * Get the resume button
	 */
	void Awake(){
		button = GetComponent<Selectable>();
	}

	/*
	 * Every Frame set if the resume button is enabled
	 */
	void Update(){
		if(button == null)
			button = GetComponent<Selectable>();
		button.interactable = GameControler.gameControler.paused;
	}
}
