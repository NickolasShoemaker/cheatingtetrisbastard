﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  OptionsMenu.cs
//  Language: C#
//
//  Description:
//  Script that controls the options in the game
//  Mostly it is just a collection of subgroups
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Class that holds the OnClick functions for the Options menu
 */
public class OptionsMenu : MonoBehaviour {

	/*
	 * When the Controls button is pressed go to controls menu
	 */
	public void OnControls() {
		SceneManager.LoadScene("ControlsMenu");
	}

	/*
	 * When the Volume button is pressed go to volume menu
	 */
	public void OnVolume() {
		SceneManager.LoadScene("VolumeMenu");
	}

	/*
	 * When the Game button is pressed go to gameplay menu
	 */
	public void OnGameplay() {
		SceneManager.LoadScene("GameMenu");
	}

	/*
	 * When the Video button is pressed go to graphics menu
	 */
	public void OnVideo() {
		SceneManager.LoadScene("VideoMenu");
	}

	/*
	 * When the back button is pressed
	 * save the game settings and go to the main menu
	 */
	public void OnBack() {
		SceneManager.LoadScene("MainMenu");
	}
}
