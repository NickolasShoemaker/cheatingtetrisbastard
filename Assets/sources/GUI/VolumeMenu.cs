﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  VolumeMenu.cs
//  Language: C#
//
//  Description:
//  Script that controls the volume of the game
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Class that controls the volume of the game
 */
public class VolumeMenu : MonoBehaviour{
	/*
	 * Set the slider values to what is saved in game settings
	 */
	void Start() {
		Slider[] sliders = FindObjectOfType<Canvas>().GetComponentsInChildren<Slider>();
		foreach(Slider slider in sliders) {
			if(slider.name == "MusicSlider")
				slider.value = GameControler.gameControler.audioSource.volume;
			else if(slider.name == "SfxSlider")
				slider.value = GameControler.gameControler.gameSettings.sfxVolume;
		}
	}

	/*
	 * When the Music slider is changed save the new value
	 */
	public void MusicChanged(float value) {
		GameControler.gameControler.audioSource.volume = value;
	}

	/*
	 * When the SFX slider is changed save the new value
	 */
	public void SfxChanged(float value) {
		GameControler.gameControler.gameSettings.sfxVolume = value;
	}

	/*
	 * When the back button is pressed
	 * save the game settings and go to the main menu
	 */
	public void OnBack() {
		GameControler.gameControler.Save();
		SceneManager.LoadScene("MainMenu");
	}
}
