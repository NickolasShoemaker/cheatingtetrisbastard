﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  ControlsMenu.cs
//  Language: C#
//
//  Description:
//  Script that controls the controls for the entire game
// ******************************

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * Logic for handling the controls menu
 * This is a Unity scene that lets us alter the
 * keys in Game Controller
 */
public class ControlsMenu : MonoBehaviour {
    // Flag to signal that we are changing a control
	bool setrotate = false;
	bool setdown = false;
	bool setleft = false;
	bool setright = false;
	bool setfastDown = false;
	bool setmainMenu = false;

	// The text of the button we are currently acting on
	Text aButton;

	/*
	 * Populate the buttons with the current control config
	 * set in Game Controller
	 */
	void Start() {
		SetActiveButton("RotateText");
		aButton.text = GameControler.gameControler.gameSettings.rotateKey.ToString();
		SetActiveButton("RightText");
		aButton.text = GameControler.gameControler.gameSettings.downKey.ToString();
		SetActiveButton("LeftText");
		aButton.text = GameControler.gameControler.gameSettings.leftKey.ToString();
		SetActiveButton("DownText");
		aButton.text = GameControler.gameControler.gameSettings.rightKey.ToString();
		SetActiveButton("FastDownText");
		aButton.text = GameControler.gameControler.gameSettings.fastDownKey.ToString();
		SetActiveButton("MainMenuText");
		aButton.text = GameControler.gameControler.gameSettings.mainMenuKey.ToString();
	}

	/*
	 * Every frame check if we should go back or change a control
	 */
    void Update(){
		if(Input.GetKeyDown(GameControler.gameControler.gameSettings.mainMenuKey)) {
			OnBack();
		}

		if(aButton == null)
			return;

    	if(setrotate){
			ChooseNewKey(ref setrotate, ref GameControler.gameControler.gameSettings.rotateKey);
		}else if(setdown){
			ChooseNewKey(ref setdown, ref GameControler.gameControler.gameSettings.downKey);
		}else if(setleft){
			ChooseNewKey(ref setleft, ref GameControler.gameControler.gameSettings.leftKey);
		}else if(setright){
			ChooseNewKey(ref setright, ref GameControler.gameControler.gameSettings.rightKey);
		}else if(setfastDown){
			ChooseNewKey(ref setfastDown, ref GameControler.gameControler.gameSettings.fastDownKey);
		}else if(setmainMenu){
			ChooseNewKey(ref setmainMenu, ref GameControler.gameControler.gameSettings.mainMenuKey);
		}
	}

	/*
	 * If we have pressed a valid key set the new control
	 * else wait to try again next frame
	 */
	void ChooseNewKey(ref bool flag, ref KeyCode key) {
		aButton.color = Color.red;
		KeyCode pressedKey = GetPressedKey();
		if(pressedKey != KeyCode.None) {
			key = pressedKey;
			aButton.text = pressedKey.ToString();
			flag = false;
			aButton.color = Color.black;
		}else{
			flag = true;
		}
	}

	/*
	 * Figure out what key was pressed by going through the KeyCode
	 * Enum and checking if that key is down.
	 * 
	 * Note: I don't know a better way than checking every key every frame
	 * as there is no built in way to do this and anyKey() does not tell you
	 * what was pressed.
	 */
	KeyCode GetPressedKey() {
		foreach(KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))) {
			if(Input.GetKey(vKey)) {
				return vKey;
			}
		}

		return KeyCode.None;
	}

	/*
	 * Find the text of this button by its name so we can edit it
	 */
	void SetActiveButton(string str) {
		Text[] texts = FindObjectOfType<Canvas>().GetComponentsInChildren<Text>();
		for(int i = 0; i < texts.Length; i++) {
			if(texts[i].name == str)
				aButton = texts[i];
		}
	}

	/*
	 * Rotate control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnRotate() {
		SetActiveButton("RotateText");
		setrotate = true;
	}

	/*
	 * Right control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnRight() {
		SetActiveButton("RightText");
		setright = true;
	}

	/*
	 * Left control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnLeft() {
		SetActiveButton("LeftText");
		setleft = true;
	}

	/*
	 * Down control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnDown() {
		SetActiveButton("DownText");
		setdown = true;
	}

	/*
	 * Fast Down control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnFastDown() {
		SetActiveButton("FastDownText");
		setfastDown = true;
	}

	/*
	 * Main Menu control button was pressed
	 * set this button as active and set flag to change this control
	 */
	public void OnMainMenu() {
		SetActiveButton("MainMenuText");
		setmainMenu = true;
	}

	/*
	 * Back button or Main Menu key was pressed
	 * Save controls and go back
	 */
	public void OnBack() {
		GameControler.gameControler.Save();
		SceneManager.LoadScene("MainMenu");
	}

	/*
	 * Reset controls from Game Controlers Default
	 */
	public void OnDefault() {
		GameControler.gameControler.gameSettings.DefaultKeys();
	}
}
