﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  CameraScript.cs
//  Language: C#
//
//  Description:
//  Script that draws the score on the screen
// ******************************

using UnityEngine;
using UnityEngine.UI;

/*
 * This class draws the score on the screen
 */
public class CameraScript : MonoBehaviour {

	Text text;

	/*
	 * Get the UI element
	 */
	void Awake() {
		text = gameObject.GetComponent<Text>();
	}

	/*
	 * Every logic tick update the score
	 */
	void FixedUpdate(){
		if(text == null)
			text = gameObject.GetComponent<Text>();
		text.text = "Score: " + GameControler.gameControler.gameSettings.score.ToString();
	}
}