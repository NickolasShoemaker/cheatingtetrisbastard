﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  VideoMenu.cs
//  Language: C#
//
//  Description:
//  Script that controls the graphics settings for the entire game
// ******************************

using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * Class that controls the graphics settings of the game
 */
public class VideoMenu : MonoBehaviour {
	
	/*
	 * Set the values of the UI elements to the values in the game settins
	 */
	void Start() {
		Toggle toggle = FindObjectOfType<Canvas>().GetComponentInChildren<Toggle>();
		toggle.isOn = Screen.fullScreen;

		Dropdown dropDown = FindObjectOfType<Canvas>().GetComponentInChildren<Dropdown>();

		List<string> options = new List<string>();
		for(int i = 0; i < Screen.resolutions.Length; i++){
			options.Add(Screen.resolutions[i].ToString());
			if(GameControler.gameControler.gameSettings.resolutionIndex == -1 && Screen.resolutions[i].ToString() == Screen.currentResolution.ToString())
				GameControler.gameControler.gameSettings.resolutionIndex = i;
		}
		dropDown.AddOptions(options);
		dropDown.value = GameControler.gameControler.gameSettings.resolutionIndex;
	}

	/*
	 * When the toggle for fullscreen is clicked
	 * Set the new value
	 */
	public void OnFullScreenToggle(bool value) {
		Screen.fullScreen = value;
		GameControler.gameControler.gameSettings.fullScreen = value;
	}

	/*
	 * When the resolution dropdown changes value set a new resolution
	 */
	public void OnResolutionChanged(int value) {
		Screen.SetResolution(Screen.resolutions[value].width, Screen.resolutions[value].height, Screen.fullScreen);
		GameControler.gameControler.gameSettings.resolutionIndex = value;
	}

	/*
	 * When the back button is pressed
	 * save the game settings and go to the main menu
	 */
	public void OnBack() {
		GameControler.gameControler.Save();
		SceneManager.LoadScene("MainMenu");
	}
}
