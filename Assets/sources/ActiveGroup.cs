﻿//  Nickolas Shoemaker 2017
//  Project: CTB
//  ActiveGroup.cs
//  Language: C#
//
//  Description:
//  Script that controls the behavior of the currently active piece
// ******************************

using UnityEngine;

/*
 * Class that controls the currently falling piece
 */
public class ActiveGroup : MonoBehaviour {
	/*
	 * We don't make blocks children of the piece as it only exists to
	 * handle collision, rotation, and falling. After that we destroy
	 * piece and migrate the blocks over to the grid.
	 */
    public GameObject[] blocks;

	/*
	 * We need these to position the blocks relative to the piece
	 * based on piece type and then to keep track of them for
	 * placing and rotation.
	 */
	Vector3[] blockOffset;

    float fallTime;				// The amount of time passed since last gravity tick
	float gravityTick = 0.3f;	// The amount of time that must pass for the active block to fall one space

	// Is right or left being held down
	bool movingLeft;
	bool movingRight;

    /*
     * Create a new piece inside spawner
     */
    public void CreatePiece(GameObject blockPrefab){
		blockOffset = new Vector3[4];

		/*
		 * There are 7 types of pieces we choose one randomly and
		 * set the offsets to make the piece that shape
		 */
		switch(Random.Range(0, 7)){
			case 0:
				blockOffset[0] = Vector3.left + Vector3.up;
				blockOffset[1] = Vector3.up;
				blockOffset[2] = new Vector3 (0, 0, 0);
				blockOffset[3] = Vector3.right;
				break;
			case 1:
				blockOffset[0] = Vector3.up;
				blockOffset[1] = Vector3.up + Vector3.right;
				blockOffset[2] = new Vector3 (0, 0, 0);
				blockOffset[3] = Vector3.right;
				break;
			case 2:
				blockOffset[0] = Vector3.up + Vector3.up;
				blockOffset[1] = Vector3.up;
				blockOffset[2] = new Vector3 (0, 0, 0);
				blockOffset[3] = Vector3.right;
				break;
			case 3:
				blockOffset[0] = Vector3.up + Vector3.up + Vector3.up;
				blockOffset[1] = Vector3.up + Vector3.up;
				blockOffset[2] = Vector3.up;
				blockOffset[3] = new Vector3 (0, 0, 0);
				break;
			case 4:
				blockOffset[0] = Vector3.right + Vector3.up + Vector3.up;
				blockOffset[1] = Vector3.right + Vector3.up;
				blockOffset[2] = Vector3.right;
				blockOffset[3] = new Vector3 (0, 0, 0);
				break;
			case 5:
				blockOffset[0] = Vector3.left;
				blockOffset[1] = Vector3.up;
				blockOffset[2] = new Vector3 (0, 0, 0);
				blockOffset[3] = Vector3.right + Vector3.up;
				break;
			case 6:
				blockOffset[0] = Vector3.left + Vector3.up;
				blockOffset[1] = Vector3.up;
				blockOffset[2] = new Vector3 (0, 0, 0);
				blockOffset[3] = Vector3.right + Vector3.up;
				break;
			default:
				Debug.LogError ("Trying to create a piece not 0 - 6.");
				break;
		}

		blocks = new GameObject[4];
		for(int i = 0; i < 4; i++) {
			blocks[i] = Instantiate(blockPrefab, blockOffset[i], Quaternion.identity);
		}

		// Don't destroy blocks on scene change
        foreach(GameObject block in blocks)
            DontDestroyOnLoad(block);
    }

	/*
	 * Since blocks are not children we need to destroy them manually before destroying ourself
	 */
    public void DestroyPiece(){
        foreach(GameObject block in blocks){
            Destroy(block);
        }
		Destroy(gameObject);
    }

	/*
	 * When this piece is created set it to not unload if we change scenes
	 */
	void Awake(){
		DontDestroyOnLoad(this);
	}

	/*
	 * When the script starts get gravity tick from Game Controler
	 */
	void Start() {
		gravityTick = GameControler.gameControler.gameSettings.speedValue;
	}

	/*
     * Move the piece every update, rotate, side to side, or drop down
     */ 
	void Update(){
		// Left key being held?
		if(Input.GetKeyDown(GameControler.gameControler.gameSettings.leftKey))
			movingLeft = true;
		else if(Input.GetKeyUp(GameControler.gameControler.gameSettings.leftKey))
			movingLeft = false;

		// Right key being held?
		if(Input.GetKeyDown(GameControler.gameControler.gameSettings.rightKey))
			movingRight = true;
		else if(Input.GetKeyUp(GameControler.gameControler.gameSettings.rightKey))
			movingRight = false;

		// Move left, right, rotate, or fast down this frame
		if((movingLeft && Time.time - fallTime >= gravityTick) || Input.GetKeyDown(GameControler.gameControler.gameSettings.leftKey)){
            if(!GameControler.gameControler.tetrisBoard.CollideLeft(this))
                MovePosition(Vector3.left);
		}else if((movingRight && Time.time - fallTime >= gravityTick) || Input.GetKeyDown(GameControler.gameControler.gameSettings.rightKey)){
            if(!GameControler.gameControler.tetrisBoard.CollideRight(this))
                MovePosition(Vector3.right);
        }else if(Input.GetKeyDown(GameControler.gameControler.gameSettings.rotateKey)){
            Rotate();
        }else if(Input.GetKeyDown(GameControler.gameControler.gameSettings.fastDownKey)){
            gravityTick = 0.002f;
        }

		// If we press down or it is time for a gravity tick move down this frame
        if(Input.GetKeyDown(GameControler.gameControler.gameSettings.downKey) || Time.time - fallTime >= gravityTick){
            if(GameControler.gameControler.tetrisBoard.Collision(this)){
                GameControler.gameControler.tetrisBoard.AddPiece(this);
                FindObjectOfType<Spawner>().SpawnNext();
                enabled = false;
                Destroy(gameObject);
            }else{
                MovePosition(Vector3.down);
            }

            fallTime = Time.time;
        }
	}

	/*
     * Set postion of blocks make piece position that of pivot block
     */
    public void SetPosition(Vector3 pos){
        transform.position = pos;
		for(int i = 0; i < 4; i++) {
			blocks[i].transform.position = pos + blockOffset[i];
		}
    }

    /*
     * Move postion of blocks keeping current offset and rotation
     */
    public void MovePosition(Vector3 pos){
        transform.position += pos;
        foreach(GameObject block in blocks){
            block.transform.position += pos;
        }
    }

    /*
     * Rotate around the pivot block
     */
    public void Rotate(){
        transform.RotateAround(transform.position, Vector3.forward, -90);
        foreach(GameObject block in blocks){
            block.transform.RotateAround(transform.position, Vector3.forward, -90);
        }

        //If we failed rotate back
        if(!GameControler.gameControler.tetrisBoard.PlaceRotation(this)){
            transform.RotateAround(transform.position, Vector3.forward, 90);
            foreach(GameObject block in blocks){
                block.transform.RotateAround(transform.position, Vector3.forward, 90);
            }    
        }
    }
}
